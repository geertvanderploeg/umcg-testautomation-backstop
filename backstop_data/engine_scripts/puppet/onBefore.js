module.exports = async (page, scenario, vp) => {
    await page.setDefaultNavigationTimeout(0);
    await require('./loadCookies')(page, scenario);
    await page.evaluateOnNewDocument(() => {
        localStorage.setItem('hideNewDesignPopup', true);
    });
};