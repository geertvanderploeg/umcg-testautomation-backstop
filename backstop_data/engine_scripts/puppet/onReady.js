module.exports = async (page, scenario, vp) => {

	console.log('SCENARIO > ' + scenario.label);

	//wait for fonts to be loaded
	await page.waitForFunction(() => {
		return document.fonts.ready.then(() => {
			//console.log('Fonts are ready');
			return true;
		});
	});

	await require('./clickAndHoverHelper')(page, scenario);
};
