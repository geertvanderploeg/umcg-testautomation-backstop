#!/bin/sh
report='no'
command='unknown'
config='unknown'
filter='.*'
error=0

case $1 in
  reference)
    command=reference
    ;;
  test)
    command=test
    ;;
  test+report)
    command=test
    report='yes'
    ;;
  approve)
    command=approve
    ;;
  *)
    error=1
    ;;
esac

case $2 in
  all)
    config='all'
    ;;
  zorg)
    config='zorg'
    ;;
  werkenbij)
    config='werkenbij'
    ;;
  research)
    config='research'
    ;;
  onderwijs)
    config='onderwijs'
    ;;
  *)
    error=1
    ;;
esac

if [ ! -z "$3" ]; then
	filter=$3
fi
clear

echo "========================================================================"
echo "Starting with these settings:"
echo "------------------------------------------------------------------------"
echo "Command:     $command"
echo "Config:      $config"
echo "OpenReport:  $report"
echo "Filter:      $filter"
echo "========================================================================"


if [ "$error" = 1 ]; then
  echo "ERROR: Could not start, invalid or incomplete parameters"
  echo "Use ./runBackstop.sh with these parameters:"
  echo " #1 reference|test|test+report|approve"
  echo " #2 all|zorg|werkenbij|research|onderwijs"
  echo " #3 filter text (optional)"
  echo "========================================================================"
  exit
fi



if [ "$config" = "all" ] || [ "$config" = "zorg" ]; then
  echo "========================================================================"
  echo "Starting backstopjs $command for ZORG"
  echo "========================================================================"
  backstop $command --config=zorg.json --filter=$filter
    if [ "$report" = 'yes' ] && [ "$command" = "test" ]; then
    backstop openReport --config=zorg.json
    tput bel
    fi
fi

if [ "$config" = "all" ] || [ "$config" = "werkenbij" ]; then
  echo "========================================================================"
  echo "Starting backstopjs $command for WERKENBIJ"
  echo "========================================================================"
  backstop $command --config=werkenbij.json --filter=$filter
    if [ "$report" = 'yes' ] && [ "$command" = "test" ]; then
    backstop openReport --config=werkenbij.json
    tput bel
    fi
fi

if [ "$config" = "all" ] || [ "$config" = "research" ]; then
  echo "========================================================================"
  echo "Starting backstopjs $command for RESEARCH"
  echo "========================================================================"
  backstop $command --config=research.json --filter=$filter
    if [ "$report" = 'yes' ] && [ "$command" = "test" ]; then
    backstop openReport --config=research.json
    tput bel
    fi
fi

if [ "$config" = "all" ] || [ "$config" = "onderwijs" ]; then
  echo "========================================================================"
  echo "Starting backstopjs $command for ONDERWIJS"
  echo "========================================================================"
  backstop $command --config=onderwijs.json --filter=$filter
    if [ "$report" = 'yes' ] && [ "$command" = "test" ]; then
    backstop openReport --config=onderwijs.json
    tput bel
    fi
fi

echo "========================================================================"
echo "Finished with these settings:"
echo "------------------------------------------------------------------------"
echo "Command:     $command"
echo "Config:      $config"
echo "OpenReport:  $report"
echo "Filter:      $filter"
echo "========================================================================"
